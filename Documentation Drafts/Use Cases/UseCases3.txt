﻿UC12: Process View Customer Log
Primary Actors: Cashier
Stakeholders & Interests: Cashier, DVD Store Owner
Preconditions: Customer Log has been created and kept appropriately.
Postconditions: Cashier is updated on both successful and unsuccessful Customer Account-related activities.


Main Success Scenario (Basic Flow):


1. Cashier enters the system.
2. Cashier goes to customer log.
3. System returns customer log file to cashier.
4. Cashier views customer log file and is updated on Customer Account-related activities.


Alternative Scenario:
Process Create Customer Log
Preconditions: Customer Log is not yet created.
Postconditions: Customer Log is created.


1. Cashier enters the system to check Customer Log.
2. Cashier finds out that Customer Log has not yet been created.
3. Cashier creates a file in the system, where all Customer Account-related activities are to be saved.
4. A record of Customer Account-related activities is kept from there on and all new activities are automatically saved.




UC13: Process View DVD Log
Primary Actor: Cashier
Stakeholders and Interests: Cashier, DVD Store Owner
Preconditions: DVD Log has been created and kept appropriately.
Postconditions: DVD is updated on both successful and unsuccessful DVD related activities.


Main Success Scenario (Basic Flow):


1. Cashier enters the system.
2. Cashier goes to dvd log.
3. System returns dvd log file to cashier.
4. Cashier views dvd log file and is updated on DVD-related activities.


Alternative Scenario:
Process Create DVD Log
Preconditions: DVD Log has not been created.
Postconditions: DVD Log is created.


1. Cashier enters the system to check DVD Log.
2. Cashier finds out that Customer Log has not yet been created.
3. Cashier creates a file in the system, where all DVD-related activities are to be saved.
4. A record of DVD-related activities is kept from there on and all new activities are automatically saved.




UC14: Process View Rentals (Weekly/Monthly)
Primary Actor: Cashier
Stakeholders and Interests: Cashier, DVD Store Owner
Preconditions: There is a number of DVD copies rented.
Postconditions: Cashier is updated on which DVDs were rented within a given amount of time (week/month). 


Main Success Scenario (Basic Flow):


1. Cashier enters the system to view current rentals.
2. Cashier enters a date.
3. System searches for rentals according to the input date, either by month and year or by week, month and year.
4. All DVD copies rented at the time are being displayed, along with who rented them.




UC15: Retrieve Top 20 Customers
Primary Actor: Cashier
Stakeholders and Interests: Cashier, DVD Store Owner
Preconditions: DVD Store has more than 20 customers.
Postconditions: A report on the top 20 customers is now created and kept.


Main Success Scenario (Basic Flow):


1. Cashier enters the system.
2. Cashier chooses to view the top 20 customers within a certain period of time.
3. System validates that the number of customers equals/is more than 20 within that  period of time.
4. System retrieves top 20 customers for that period of time.
5. A report is being made and recorded to a file.


Alternative Scenario:
Preconditions: DVD Store has less than 20 customers.
Postconditions: System returns a message informing the cashier accordingly. An appropriate report file is created.


1. Cashier enters the system.
2. Cashier chooses to view the top 20 customers within a certain period of time.
3. System validates that the number of customers is less than 20 within that period of time.
4. System returns a message  informing the cashier accordingly, which is then recorded to a report file.




UC16: Process Retrieve Best Selling Films
Primary Actor: Cashier
Stakeholders and Interests: Cashier, DVD Store Owner
Preconditions: More than ten films must have been already rented.
Postconditions: A report on the ten best selling films of all time is created and kept.


Main Success Scenario (Basic Flow):


1. Cashier enters the system.
2. Cashier chooses to view the best selling films list.
3. System validates that the number of films rented until that point of time is more than 10.
4. System retrieves the ten best selling films of all time.
5. The results produced by the system are recorded to a report file.


Alternative Scenario:
Preconditions: Less than ten films have been already rented.
Postconditions: A message is displayed informing the cashier accordingly. An appropriate report file is created.


1. Cashier enters the system.
2. Cashier chooses to view the best selling films list.
3. System validates that the number of films rented until that point of time equals/is less than 10.
4. System returns a message  informing the cashier accordingly, which is then recorded to a report file.




UC17: Process Retrieve Out Of Stock Films
Primary Actor: Cashier
Stakeholders and Interests: Cashier, DVD Store Owner
Preconditions: At least one film must have been already rented.
Postconditions: A report on currently out of stock films is created.


Main Success Scenario (Basic Flow):


1. Cashier enters the system.
2. Cashier hits search.
3. Cashier enters current date.
4. System validates whether there are any copies rented using as validation fields the id of the film and the date rented.
5. System checks whether the aforementioned copies are returned.
6. System produces a report file of copies unavailable at the time.


Alternative Scenario:
Preconditions: No films rented before.
Postconditions: A message is displayed informing the cashier accordingly.


1. Cashier enters the system.
2. Cashier hits search.
3. Cashier enters current date.
4. System validates whether there are any copies rented using as validation fields the id of the film and the date rented.
5. System returns a message  informing the cashier that 0 number of rentals have been made so far.
